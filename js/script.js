/*Global variables*/
var game = {};
game.field = [
    [],
    [],
    []
];
game.null = {
    x: 2,
    y: 2
}

$(document).ready(function() {
    initField();
})

function reInitField() {
    var itemList = $('.item');
    var existValues = [];
    for (var i = 0; i < itemList.length; i++) {
        value = getRandom(1, 10, existValues);
        $(itemList[i]).attr('data-value', value).text(value);
    }
}

function initField() {

    var itemList = $('.item');
    var existValues = [];

    for (var i = 0; i < 3; i++) {
        for (var c = 0; c < 3; c++) {
            var item = itemList[i * 3 + c],
                value = getRandom(1, 10, existValues);

            $(item).attr('data-value', value).text(value);
            $(item).attr('data-pos-x', c).attr('data-pos-y', i);

            $(item).addClass('pos-' + i + c);
        }
    }

    initLogic();

}

function initLogic() {
    $(document).ready(function() {
        $('.item').click(function() {
            if (emptyNearby($(this).attr('data-pos-x'), $(this).attr('data-pos-y'))) {
                console.log('1212');
                emptyChangePos(this);
                if (checkWin()) {
                    alert('It is win!');
                    reInitField();
                };
            }
        });
    });
}

function emptyChangePos(item) {
    var currentItemX = $(item).attr('data-pos-x'),
        currentItemY = $(item).attr('data-pos-y');

    $(item).removeClass('pos-' + currentItemY + currentItemX).addClass('pos-' + game.null.y + game.null.x);
    $(item).attr('data-pos-x', game.null.x).attr('data-pos-y', game.null.y);
    game.null.x = currentItemX;
    game.null.y = currentItemY;
}

function checkWin() {
    var itemList = $('.item'),
        res = true;

    for (var i = 1; i < itemList.length; i++) {
        if (($(itemList[i - 1]).attr('data-value') <= $(itemList[i]).attr('data-value')) && (res)) {

            res = true;
        } else {
            res = false;
        }
    };

    return (res && $('.pos-22').length === 0);
}

function emptyNearby(x, y) {
    var offsetX = Math.abs(x - game.null.x);
    var offsetY = Math.abs(y - game.null.y);
    if (offsetX <= 1 && offsetY <= 1 && offsetX !== offsetY) {
        return true;
    }
    return false;
}

function getRandom(min, max, existValues) {
    var value = Math.floor(Math.random() * (max - min) + min);
    while (existValues.indexOf(value) !== -1) {
        value = Math.floor(Math.random() * (max - min) + min);
    }
    existValues.push(value);
    return value;
}

function testWin() {
    $('.item').attr('data-value', 1);
}